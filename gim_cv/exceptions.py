import logging

log = logging.getLogger(__name__)

class InvalidArrayError(Exception):
    pass
