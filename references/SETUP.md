Installation and setup instructions
=============================================


## Docker installation instructions

### Build docker image containing the code and dependencies

Use docker-compose to build the image specified in `Dockerfile`. This creates an
image called `gim_cv` on Debian, installs Anaconda, copies the source
code of this repository into the image before finally installing python
dependencies.

```bash
docker-compose up -d
```

If you run into an error like `Couldn't connect to docker daemon`, do the
following and try again:

```
sudo systemctl unmask docker
sudo systemctl start docker
```

### Create a container from the image and launch it

Once you've run `docker-compose up -d`, a specific instance of this image
(a container) called
`gim_cv_container` is created, with the specifics configured as in
`docker-compose.yml` (port-forwarding, volume mirroring with the host OS etc.).

Check that your container is up and running:

```bash
docker ps
```

You should see something like this:

```shell
CONTAINER ID        IMAGE               COMMAND               CREATED             STATUS              PORTS                    NAMES
44315329363a        gim_cv        "tail -f /dev/null"   29 seconds ago      Up 19 seconds       0.0.0.0:8888->8888/tcp   gim_cv_container
```
###



### disable firewall (in case of problems on CentOS)

If, on Linux, you run into the issue that the containers have no internet
(eg apt update fails), do:

```bash
sudo systemctl disable firewalld
```

```bash
systemctl stop docker
systemctl start docker
```

### Attach to the container

Now attach to the container to run code in the environment:

```bash
docker exec -it gim_cv_container /bin/bash
```

Press ctrl+D or type 'exit' to exit.

## Working in docker enviroment

Activate the environment with `conda activate gim_cv`.

### Editing code

Just edit the code in the project directory in the host OS, as this is mirrored
in the container by sharing the volume.

### Jupyter Notebooks

You can use Jupyter notebook by launching it in the container, then opening
your browser at port 8888 of localhost on the host OS.

```bash
$ jupyter-notebook --ip=0.0.0.0 --no-browser --allow-root
```
This is aliased in the docker environment as simply:

```bash
$ jn
```
You should open the *second* link which appears in:

```shell
[I 12:12:17.801 NotebookApp] Serving notebooks from local directory: /home/root
[I 12:12:17.801 NotebookApp] The Jupyter Notebook is running at:
[I 12:12:17.801 NotebookApp] http://6d663593d11f:8888/?token=82d0ab41090cc42f5793528225b46f324e8cbc02d903cb85
[I 12:12:17.801 NotebookApp]  or http://127.0.0.1:8888/?token=82d0ab41090cc42f5793528225b46f324e8cbc02d903cb85
```

i.e. that which begins with http://127.0.0.1:8888/

## Install gim_cv as python package

Install `gim_cv` as a python package in editable mode. Inside the
conda environment, do:

```bash
$ pip install -e .
```

# Non-essential instructions

## Access Sentinel Data

Sign up for a free copernicus account and make a note of your username and passsword at [this link](https://scihub.copernicus.eu/dhus/#/self-registration).

Install [SentinelSat](https://sentinelsat.readthedocs.io/en/stable/) on PyPI:

`pip install sentinelsat`

Now in python, create an API session like:

`
from sentinelsat import SentinelAPI, read_geojson, geojson_to_wkt

api = SentinelAPI('username', 'password', api_url='https://scihub.copernicus.eu/dhus')
`


## Install R (Not really used -- optional!) (CentOS 8)


```bash
sudo yum install epel-release
sudo yum config-manager --set-enabled PowerTools
sudo yum install R
```

### Install RStudio

https://rstudio.com/products/rstudio/download/#download
