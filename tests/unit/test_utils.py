import pytest

@pytest.mark.skip
def test_require_attr_true():
    pass

@pytest.mark.skip
def test_hash_iterable():
    pass

@pytest.mark.skip
def test_free_from_white_pixels():
    pass

@pytest.mark.skip
def test_get_bbox_slices():
    pass

@pytest.mark.skip
def test_get_matching_image_mask_files():
    pass

@pytest.mark.skip
def test_bounding_box_from_ixs():
    pass

@pytest.mark.skip
def test_bounding_box_equals():
    pass

@pytest.mark.skip
def test_bounding_box_nonzero():
    pass

@pytest.mark.skip
def test_onehot_sparse():
    pass

@pytest.mark.skip
def test_onehot_initialization_v2():
    pass

@pytest.mark.skip
def test_ensure_chunks_aligned():
    pass

@pytest.mark.skip
def test_resize_array_to_fit_window():
    pass

@pytest.mark.skip
def test_shuffle_array():
    pass

@pytest.mark.skip
def test_split_array():
    pass

@pytest.mark.skip
def test_shuffle_and_split_array():
    pass

@pytest.mark.skip
def test_offset_by_strides():
    pass

@pytest.mark.skip
def test_stride_overlap_and_resize():
    pass

@pytest.mark.skip
def test_random_partitioned_index_permutation():
    pass

@pytest.mark.skip
def test_view_offsets():
    pass

@pytest.mark.skip
def test_cubify():
    pass

@pytest.mark.skip
def test_uncubify():
    pass

@pytest.mark.skip
def test_cubification():
    pass

@pytest.mark.skip
def test_strided_grid():
    pass

@pytest.mark.skip
def test_window_slices():
    pass

@pytest.mark.skip
def test_count_windows():
    pass

@pytest.mark.skip
def test_plot_pair():
    pass

@pytest.mark.skip
def test_plot_img_and_mask():
    pass

@pytest.mark.skip
def test_get_config_path():
    pass

@pytest.mark.skip
def test_plot_sample_segmentation():
    pass

@pytest.mark.skip
def test_plot_segmentation():
    pass

@pytest.mark.skip
def test_get_patch_origin_points():
    pass

@pytest.mark.skip
def test_count_patches():
    pass

@pytest.mark.skip
def test_get_patch_indices():
    pass

@pytest.mark.skip
def test_get_image_mask_pair_sample():
    pass

@pytest.mark.skip
def test_yield_chunks():
    pass

@pytest.mark.skip
def test_plot_masked_region():
    pass

@pytest.mark.skip
def test_img_mask_batch_generator():
    pass

@pytest.mark.skip
def test_aggregate_da():
    pass

@pytest.mark.skip
def test_registered_attribute():
    pass
