import pytest


class TestBaseInferenceDataset:
    pass

class TestCompositeInferenceDataset:
    @pytest.mark.skip
    def test_prepare(self):
        pass
    @pytest.mark.skip
    def test_schedule_inference(self):
        pass
    @pytest.mark.skip
    def test_constituent_datasets_by_tag(self):
        pass

class TestInferenceDataset:
    @pytest.mark.skip
    def test_prepare(self):
        pass
    @pytest.mark.skip
    def test_schedule_inference(self):
        pass
    @pytest.mark.skip
    def test_write_mask_raster(self):
        pass
    @pytest.mark.skip
    def test_make_pipeline(self):
        pass
