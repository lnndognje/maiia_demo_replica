import pytest


@pytest.mark.skip
def test_recall():
    pass


@pytest.mark.skip
def test_precision():
    pass


@pytest.mark.skip
def test_specificity():
    pass


@pytest.mark.skip
def test_npv():
    pass


@pytest.mark.skip
def test_tp():
    pass


@pytest.mark.skip
def test_tn():
    pass


@pytest.mark.skip
def test_calc_dist_map():
    pass


@pytest.mark.skip
def test_calc_dist_map_batch():
    pass


@pytest.mark.skip
def test_update_alpha():
    pass


@pytest.mark.skip
def test_tversky_index():
    pass


@pytest.mark.skip
def test_dice_coefficient():
    pass


@pytest.mark.skip
def test_tanimoto_coefficient():
    pass


@pytest.mark.skip
def test_jaccard_index():
    pass


@pytest.mark.skip
def test_tversky_loss():
    pass


@pytest.mark.skip
def test_tversky_loss_tunable():
    pass


@pytest.mark.skip
def test_focal_tversky_loss():
    pass


@pytest.mark.skip
def test_dice_coeff_loss():
    pass


@pytest.mark.skip
def test_bce_dice_loss():
    pass


@pytest.mark.skip
def test_class_weighted_pixelwise_crossentropy():
    pass


@pytest.mark.skip
def test_binary_crossentropy_from_logits():
    pass


@pytest.mark.skip
def test_weighted_binary_crossentropy():
    pass


@pytest.mark.skip
def test_bbce_adaptive():
    pass


@pytest.mark.skip
def test_wbce_adaptive():
    pass


@pytest.mark.skip
def test_wbce_adaptive_dice_loss():
    pass


@pytest.mark.skip
def test_weighted_wbce_adaptive_dice_loss():
    pass


@pytest.mark.skip
def test_surface_loss():
    pass


@pytest.mark.skip
def test_lovasz_grad():
    pass


@pytest.mark.skip
def test_lovasz_hinge_loss():
    pass


@pytest.mark.skip
def test_lovasz_hinge():
    pass


@pytest.mark.skip
def test_lovasz_hinge_flat():
    pass


@pytest.mark.skip
def test_flatten_binary_scores():
    pass


@pytest.mark.skip
def test_lovasz_softmax():
    pass


@pytest.mark.skip
def test_lovasz_softmax_flat():
    pass


@pytest.mark.skip
def test_flatten_probas():
    pass

class TestAlphaScheduler:
    pass
