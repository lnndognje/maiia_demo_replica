gim\_cv.scrapers.vl\_orthos package
===================================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   gim_cv.scrapers.vl_orthos.spiders

Submodules
----------

gim\_cv.scrapers.vl\_orthos.items module
----------------------------------------

.. automodule:: gim_cv.scrapers.vl_orthos.items
   :members:
   :undoc-members:
   :show-inheritance:

gim\_cv.scrapers.vl\_orthos.middlewares module
----------------------------------------------

.. automodule:: gim_cv.scrapers.vl_orthos.middlewares
   :members:
   :undoc-members:
   :show-inheritance:

gim\_cv.scrapers.vl\_orthos.pipelines module
--------------------------------------------

.. automodule:: gim_cv.scrapers.vl_orthos.pipelines
   :members:
   :undoc-members:
   :show-inheritance:

gim\_cv.scrapers.vl\_orthos.settings module
-------------------------------------------

.. automodule:: gim_cv.scrapers.vl_orthos.settings
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: gim_cv.scrapers.vl_orthos
   :members:
   :undoc-members:
   :show-inheritance:
