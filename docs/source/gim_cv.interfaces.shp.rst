gim\_cv.interfaces.shp package
==============================

Submodules
----------

gim\_cv.interfaces.shp.fiona module
-----------------------------------

.. automodule:: gim_cv.interfaces.shp.fiona
   :members:
   :undoc-members:
   :show-inheritance:

gim\_cv.interfaces.shp.ogr module
---------------------------------

.. automodule:: gim_cv.interfaces.shp.ogr
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: gim_cv.interfaces.shp
   :members:
   :undoc-members:
   :show-inheritance:
