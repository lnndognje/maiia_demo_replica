gim\_cv.scrapers.vl\_orthos.spiders package
===========================================

Submodules
----------

gim\_cv.scrapers.vl\_orthos.spiders.ortho\_cataloguer module
------------------------------------------------------------

.. automodule:: gim_cv.scrapers.vl_orthos.spiders.ortho_cataloguer
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: gim_cv.scrapers.vl_orthos.spiders
   :members:
   :undoc-members:
   :show-inheritance:
